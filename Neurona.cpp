#include "Neurona.h"
#include <iostream>
#include <math.h>
#include <time.h>
#include <cstdlib>

double Neurona::va = 1.5;
double Neurona::momento = 0.2;

Neurona::Neurona(unsigned numeroSalidas, unsigned indice){

	for(unsigned c=0; c < numeroSalidas; ++c){
		pesoSalidas.push_back(Conexion());
		//Peso aleatorio[0..1]
		pesoSalidas.back().peso = (double)rand()/RAND_MAX;
		//Bias aleatorio
		//bias = (double) rand() / (RAND_MAX);
	}

	this->indice = indice;
}

void Neurona::PonerValorSalida(double valor){
	salida = valor;
}

double Neurona::ObtenerSalida() const{
	return salida;
}

void Neurona::FeedForward(const Capa &capaAnterior){
    double suma = 0.0;


	for(unsigned n=0; n<capaAnterior.size(); ++n){
		suma += capaAnterior[n].ObtenerSalida()*capaAnterior[n].pesoSalidas[indice].peso;
	}

	//suma += bias;
	//Funcion de transferencia
	salida = Sigmoid(suma);
	//salida = Transf(suma);
}

void Neurona::CalcularGradienteSalida(double objetivo){
	double delta = objetivo-salida;
	gradiente = delta * Neurona::SigmoidDerivada(salida);
	//gradiente = delta * Neurona::TransfDerivada(salida);
}
void Neurona::CalcularGradienteOculto(const Capa &capaSiguiente){
	double se = SumaErrores(capaSiguiente);
	gradiente = se*SigmoidDerivada(salida);
	//gradiente = se*TransfDerivada(salida);
}
void Neurona::ActualizarPesos(Capa &capaAnterior){
	for(unsigned n=0; n<capaAnterior.size(); ++n){
		Neurona &ne = capaAnterior[n];
		double antiguoPesoDelta = ne.pesoSalidas[indice].pesoDelta;

		//va = Velocidad de aprendizaje
		double nuevoPesoDelta = va * ne.ObtenerSalida() *gradiente + momento * antiguoPesoDelta;

		ne.pesoSalidas[indice].pesoDelta = nuevoPesoDelta;
		ne.pesoSalidas[indice].peso += nuevoPesoDelta;
	}
}

double Neurona::Sigmoid(double x){
	return 1/(1+exp(-x));
}

double Neurona::SigmoidDerivada(double x){
	return Sigmoid(x)*(1-Sigmoid(x));
}

double Neurona::Transf(double x){
	return tanh(x);
}
double Neurona::TransfDerivada(double x){
	return 1.0 - x * x;
}

double Neurona::SumaErrores(const Capa &capaSiguiente)const{
	double suma = 0.0;
	//Sumar contribucion de errres de los nodos a los que alimentamos
	for(unsigned n=0; n<capaSiguiente.size()-1; ++n){
		suma += pesoSalidas[n].peso*capaSiguiente[n].gradiente;
	}
	return suma;
}
