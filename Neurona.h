#ifndef NEURONA_H
#define NEURONA_H
#include <vector>

using namespace std;

class Neurona;
typedef vector<Neurona> Capa;

struct Conexion{
	double peso;
	double pesoDelta;
};

class Neurona{
public:
	Neurona(unsigned numeroSalidas, unsigned indice);

	void PonerValorSalida(double valor);
	double ObtenerSalida() const;
	void FeedForward(const Capa &capaAnterior);
	void CalcularGradienteSalida(double objetivo);
	void CalcularGradienteOculto(const Capa &capaSiguiente);
	void ActualizarPesos(Capa &capaAnterior);
	
private:
	static double va; //velocidad de aprendizaje
	static double momento;
	static double Sigmoid(double x);
	static double SigmoidDerivada(double x);
	
	static double Transf(double x);
	static double TransfDerivada(double x);
	double SumaErrores(const Capa &capaSiguiente) const;

	vector<Conexion> pesoSalidas;
	double salida;
	unsigned indice;
	double gradiente;
	//double bias;
};



#endif
