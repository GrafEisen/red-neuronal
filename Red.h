#ifndef RED_H
#define RED_H
#include <vector>
#include <string>
#include "Neurona.h"

using namespace std;
//class Neurona;

class RedNeuronal{
public:
	/** Constructor de red
	*	@param[in] topologia Vector que contiene la cantidad de neuronas por capa
	*	Ej: [3,2,1]
	*/
	RedNeuronal(const vector<unsigned> &topo);

	void FeedForward(const vector<double> &datos);

	void BackPropagation(const vector<double> &datos);

	void ObtenerResultados(vector<double> &resultados) const;

	void Entrenar(vector<vector<double> > datos, vector<unsigned char> numeros, unsigned cantidad);

    void TransformarNumero(const unsigned char n, vector<double> &a) const;

	void MostrarVector(string nombre, vector<double> vector) const;

	double ObtenerErrorPromedio() const;

private:
	vector<Capa> capas;  //capas[numeroCapa][numeroNeurona]

	double error;
	double promErrorReciente;
	double suavisadoError;


};

#endif
