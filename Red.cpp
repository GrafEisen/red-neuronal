#include "Red.h"
#include <time.h>
#include <iostream>
#include <cstdlib>
#include <math.h>
#include <cassert>
#include <string>

using namespace std;


RedNeuronal::RedNeuronal(const vector<unsigned> &topo){
	error = 0.0;
	promErrorReciente = 0.0;
	suavisadoError = 0.0;
	//this->topo = topo;
	srand((unsigned int)time(NULL));

	for(unsigned i=0; i<topo.size(); ++i){
		capas.push_back(Capa());

		unsigned numeroSalidas = (i == topo.size()-1) ? 0 : topo[i+1];
		//float bias = ((float) rand() / (RAND_MAX));
		for(unsigned j=0; j<=topo[i]; ++j){
			capas.back().push_back(Neurona(numeroSalidas,j));
			std::cout<<"Neurona Creada!\n";
		}
		capas.back().back().PonerValorSalida(1.0);
	}

}

void RedNeuronal::FeedForward(const vector<double> &datos){
	//Verificar que la entrada es igual a la cantidad de neuronas de entrada
	assert(datos.size() == capas[0].size()-1);

	//Asignar valores a las neuronas de entrada
	for( unsigned i=0; i< datos.size(); ++i){
		capas[0][i].PonerValorSalida(datos[i]);
	}

	//Propagar hacia adelante

	for(unsigned nCapa = 1; nCapa < capas.size(); ++nCapa){
		Capa &capaAnterior = capas[nCapa-1];
		#pragma omp parallel for //num_threads(4)
		for(unsigned n = 0; n<capas[nCapa].size()-1; ++n){
            //Capa &capaAnterior = capas[nCapa-1];
			capas[nCapa][n].FeedForward(capaAnterior);
		}
	}
}

void RedNeuronal::BackPropagation(const vector<double> &datos){
	//Calcular error general de la red (RMS de salida de neuronas)
	Capa &capaSalida = capas.back();
	error = 0.0;

	for(unsigned n=0; n<capaSalida.size()-1; ++n){
		double delta = datos[n] - capaSalida[n].ObtenerSalida();
		error += delta*delta;
	}
	error /= capaSalida.size()-1;
	error = sqrt(error);

	//Implementar un calculo de promedio reciente
	promErrorReciente = (promErrorReciente*suavisadoError+error)/(suavisadoError+1);

	//Calcular gradiente de neuronas de salida
	for(unsigned n=0; n<capaSalida.size()-1; ++n){
		capaSalida[n].CalcularGradienteSalida(datos[n]);
	}

	for(unsigned nCapa=capas.size()-2; nCapa>0 ; --nCapa){
		Capa &capaOculta = capas[nCapa];
		Capa &capaSiguiente = capas[nCapa+1];

		for(unsigned n=0; n<capaOculta.size(); ++n){
			capaOculta[n].CalcularGradienteOculto(capaSiguiente);
		}
	}

	//Para todas las capas actualizar el peso de las conexiones
	for(unsigned nCapa = capas.size()-1; nCapa>0 ; --nCapa){
		Capa &capa = capas[nCapa];
		Capa &capaAnterior = capas[nCapa-1];

		for(unsigned n=0; n<capa.size()-1;++n){
			capa[n].ActualizarPesos(capaAnterior);
		}
	}
}

void RedNeuronal::ObtenerResultados(vector<double> &resultados) const{
	resultados.clear();

	for(unsigned n=0; n<capas.back().size()-1; ++n){
		resultados.push_back(capas.back()[n].ObtenerSalida());
	}
}

void RedNeuronal::Entrenar(vector<vector<double> > datos, vector<unsigned char> numeros, unsigned cantidad){
	vector<double> salida;
	vector<double> esperado;
	for(int i=0 ;i<cantidad; ++i){
		//Mostrar entradas
		//MostrarVector("Entradas:",datos[i]);
		//Alimentar red
		FeedForward(datos[i]);
		//Recolectar salida
		ObtenerResultados(salida);
		MostrarVector("Salidas:",salida);
		//Entrenar red para ver cual deberia haber sido el resultado
		TransformarNumero(numeros[i],esperado);
		MostrarVector("Objetivo:",esperado);

		BackPropagation(esperado);

		//Reportar que tan bien esta el entrenamiento
		cout << "Error promedio de la red: "<< ObtenerErrorPromedio() <<endl;
	}

	 cout<<"Entrenamiento finalizado"<<endl;

}

void RedNeuronal::TransformarNumero(const unsigned char n, vector<double> &a) const{
	a.clear();
	//Transformar numero a salida
	for(int i=0; i<9; ++i){
		if(i==n)
			a.push_back(1);
		else
			a.push_back(0);
	}
}

void RedNeuronal::MostrarVector(string nombre, vector<double> vector) const{
	cout<<nombre<<" ";
	for(int i=0; i<vector.size(); ++i){
		cout<<vector[i]<<" ";
	}
	cout<<endl;
}

double RedNeuronal::ObtenerErrorPromedio() const{
	return promErrorReciente;
}
