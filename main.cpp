#include "Red.h"
#include "ImportarMNIST.h"
#include <omp.h>
#include <stdio.h>



using namespace std;



int main(){

	vector<unsigned> T;

	T.push_back(784);
	T.push_back(15);
	T.push_back(9);

	RedNeuronal A(T);

	unsigned nImagenes = 6000;

    vector<vector<double> > datos;
    vector<unsigned char> numeros;
    ReadMNIST(nImagenes,28*28,datos);
    ReadLabels(nImagenes,numeros);

    uint64 inicio = GetTimeMs64();
    A.Entrenar(datos,numeros,nImagenes);
    uint64 fin = GetTimeMs64();

    cout<<"Timepo de entrenamiento: "<<fin-inicio<<"ms"<<endl;


	cin.get();
}
