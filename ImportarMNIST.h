#include <iostream>
#include <fstream>
#include <vector>
#include <omp.h>
#include <iomanip>
#include "Tiempo.h"

#define PARALELO 0

using namespace std;
int ReverseInt (int i)
{
    unsigned char ch1, ch2, ch3, ch4;
    ch1=i&255;
    ch2=(i>>8)&255;
    ch3=(i>>16)&255;
    ch4=(i>>24)&255;
    return((int)ch1<<24)+((int)ch2<<16)+((int)ch3<<8)+ch4;
}
void ReadLabels(int NumberOfImages, vector<unsigned char> &vec){
    uint64 inicio = GetTimeMs64();
    cout<<"Iniciando carga"<<endl;
    vec.resize(NumberOfImages);
    ifstream file ("train-labels.idx1-ubyte", ios::binary);
    if (file.is_open()){
        int magic_number = 0;
        int number_of_images = 0;
        int n_rows = 0;
        int n_cols = 0;
        file.read((char*) &magic_number, sizeof(magic_number));
        magic_number = ReverseInt(magic_number);
        file.read((char*) &number_of_images,sizeof(number_of_images));
        number_of_images = ReverseInt(number_of_images);
        if(PARALELO){
            #pragma omp parallel for ordered
            for(int i = 0; i < NumberOfImages/*number_of_images*/; ++i){
                #pragma omp ordered
                {
                    unsigned char temp = 0;
                    file.read((char*) &temp, sizeof(temp));
                    vec[i]= temp;
                }
            }
        }else{
            for(int i = 0; i < NumberOfImages/*number_of_images*/; ++i){
                unsigned char temp = 0;
                file.read((char*) &temp, sizeof(temp));
                vec[i]= temp;
            }
        }
    }
    uint64 fin = GetTimeMs64();
    cout<<"Carga finalizada"<<endl;

    cout<<"Timepo de carga: "<<setprecision(5)<<fin-inicio<<"ms"<<endl;
    //cin.get();
}


void ReadMNIST(int NumberOfImages, int DataOfAnImage,vector<vector<double>> &arr){
    //arr.resize(NumberOfImages,vector<float>(DataOfAnImage));
    arr.resize(NumberOfImages);
    for(unsigned i=0; i<arr.size(); ++i)
        arr[i].resize(DataOfAnImage);

    ifstream file ("train-images.idx3-ubyte",ios::binary);
    if (file.is_open()){
        int magic_number=0;
        int number_of_images=0;
        int n_rows=0;
        int n_cols=0;
        file.read((char*)&magic_number,sizeof(magic_number));
        magic_number= ReverseInt(magic_number);
        file.read((char*)&number_of_images,sizeof(number_of_images));
        number_of_images= ReverseInt(number_of_images);
        file.read((char*)&n_rows,sizeof(n_rows));
        n_rows= ReverseInt(n_rows);
        file.read((char*)&n_cols,sizeof(n_cols));
        n_cols= ReverseInt(n_cols);
        for(int i=0;i<NumberOfImages/*number_of_images*/;++i)
        {
            //vector<double> tp;
            for(int r=0;r<n_rows;++r)
            {
                for(int c=0;c<n_cols;++c)
                {
                    unsigned char temp=0;
                    file.read((char*)&temp,sizeof(temp));
                    arr[i][(n_rows*r)+c]= (float)temp;
                    //tp.push_back((double)temp/255);
                }
            }
        //arr.push_back(tp);
        }
    }
}
